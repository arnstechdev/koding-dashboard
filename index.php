<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="assets/img/icon-2.png">

    <title>Dashboard Login</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this tempslate -->
    <link href="dist/css/signin.css" rel="stylesheet">
  </head>

  <body class="text-center bg">
    <div class="container">
    <div class="row">
      <div class="col col-lg-4">
        <!--  -->
      </div>
      <div class="col col-lg-4">
        <div class="card" style="width: 25rem;">
        <form class="form-signin" action="cek_login.php" method="post">
          <img class="mb-4" src="assets/images/logo.png" alt="" width="130" height="130">
          <h1 class="h3 mb-3 font-weight-normal">Administrator</h1>
          <label for="inputUsername" class="sr-only">username</label>
          <input type="text" name="username" id="username" class="form-control" placeholder="Username" required autofocus><hr>
          <label for="inputPassword" class="sr-only">Password</label>
          <input type="password" name="password" id="password" class="form-control" placeholder="Password" required>
          <!-- cek pesan notifikasi -->

            <?php
          	if(isset($_GET['message'])){
          		if($_GET['message'] == "login_gagal"){
          			echo "<div class='info alert alert-danger' role='alert'>Username atau password salah!</div>";
          		}else if($_GET['message'] == "logout"){
          			echo "<div class='info alert alert-warning' role='alert'>Anda telah berhasil logout</div>";
          		}else if($_GET['message'] == "harap_login"){
          			echo "<div class='info alert alert-warning' role='alert'>Anda harus login untuk mengakses halaman admin</div>";
          		}
          	}
          	?>
          <button class="btn btn-lg btn-primary btn-block" type="submit" data-toggle="tooltip" data-placement="top" title="Klik untuk masuk Dashboard">Masuk</button>
          <p class="mt-5 mb-3 text-muted">&copy; 2017-2018</p>
        </form>
        </div>
      </div>
      <div class="col col-lg-4">
        <!--  -->
      </div>
    </div>
  </div>
  </body>
</html>
