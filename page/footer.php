<div class="card text-center">
  <div class="card-body">
    <h5 class="">Sosial Media :</h5>
    <a href="" class="btn" style="color: #4064AC;"><i class="fab fa-facebook-f"></i> Facebook</a>
    <a href="" class="btn" style="color: #D02975;"><i class="fab fa-instagram"></i> Instagram</a>
    <a href="" class="btn" style="color: #4EC35D;"><i class="fab fa-whatsapp"></i> WhatsApp Chat</a>
    <a href="" class="btn" style="color: #2BA0D9;"><i class="fab fa-instagram"></i> Telegram Chat</a>
  </div>
  <div class="card-footer text-muted">
    <i class="fas fa-code"></i> with <i class="fas fa-heart"></i> + <i class="fas fa-coffee"></i> By ArnstechDev
  </div>
</div>
