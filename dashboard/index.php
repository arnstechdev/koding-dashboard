<!doctype html>
<html lang="en">
  <head>
    <!-- Favicon -->
    <link rel="shortcut icon" href="assets/img/icon-2.png">
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet">
    <!-- CSS -->
    <link rel="stylesheet" href="../vendor/fontawesome/css/all.css">
    <link rel="stylesheet" href="../vendor/bootstrap/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="../dist/css/admin.css">
    <title>Dashboard-App</title>
    <script src="../vendor/particlesjs/particles.min.js"></script>
    <script src="../dist/js/chart.min.js"></script>
  </head>
  <body>
    <div class="wrapper">
      <!-- Navbar -->
      <nav class="navbar fixed-top navbar-light" style="background-color: #33a1fd;">
        <a class="navbar-brand" href="index.php" style="color: white;">
          <img src="../assets/img/icon-2.png" width="30" height="30" class="d-inline-block align-top" alt=""> Dashboard</a>
      </nav>
      <!-- Aside -->
      <aside class="sidebar">
        <ul class="menu-content">
          <li>
            <a href="index.php" data-toggle="tooltip" data-placement="right" title="Dashboard"><i data-feather="monitor"></i> <span class="hidden-menu">Dashboard</span></a>
          </li>
          <li>
            <a href="" data-target="#menu" data-toggle="collapse" data-placement="right" title="Data Master"><i data-feather="database"></i> <span class="hidden-menu">Data Master</span> <i class="fas fa-angle-down hidden-menu"></i></a>
            <ul id="menu" class="collapse drop-menu">
              <li><a href="index.php?page=karyawan" data-toggle="tooltip" data-placement="right" title="Data Karyawan"><i data-feather="users" class="hidden-drop-icon"></i> <span class="hidden-drop">Data Karyawan</span></a></li>
              <li><a href="index.php?page=client" data-toggle="tooltip" data-placement="right" title="Data Klien"><i data-feather="archive" class="hidden-drop-icon"></i> <span class="hidden-drop">Data Klien</span></a></li>
            </ul>
          </li>
          <li>
            <a href="index.php?page=absensi" data-toggle="tooltip" data-placement="right" title="Absensi"><i data-feather="file-text"></i> <span class="hidden-menu">Absensi</span></a>
          </li>
          <li>
            <a href="" data-target="#menu-settings" data-toggle="collapse" data-placement="right" title="Pengaturan"><i data-feather="settings"></i> <span class="hidden-menu">Pengaturan</span> <i class="fas fa-angle-down hidden-menu"></i></a>
            <ul id="menu-settings" class="collapse drop-menu">
              <li><a href="index.php?page=akun" data-toggle="tooltip" data-placement="right" title="Akun"><i data-feather="user" class="hidden-drop-icon"></i> <span class="hidden-drop">Akun</span></a></li>
              <li><a href="#" data-toggle="tooltip" data-placement="right" title="Ubah Kata sandi"><i data-feather="lock" class="hidden-drop-icon"></i> <span class="hidden-drop">Ubah Kata sandi</span></a></li>
              <li><a href="logout.php" data-toggle="tooltip" data-placement="right" title="Keluar"><i data-feather="log-out" class="hidden-drop-icon"></i> <span class="hidden-drop">Keluar<span></a></li>
            </ul>
          </li>
        </ul>
      </aside>
      <!-- Content -->
      <section class="content">
        <div class="inner">
          <?php
            if (isset($_GET['page'])) {
              if ($_GET['page']=="karyawan") {
                include '../page/karyawan.php';
              }
              elseif ($_GET['page']=="client") {
                include '../page/client.php';
              }
              elseif ($_GET['page']=="absensi") {
                include '../page/absensi.php';
              }
              elseif ($_GET['page']=="akun") {
                include '../page/akun.php';
              }
            }
            else {
              include '../page/home.php';
            }
           ?>
        </div>
      </section>
    </div>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="../dist/js/admin.js"></script>
    <script src="../dist/js/jquery.min.js"></script>
    <script src="../dist/js/popper.js"></script>
    <script src="../vendor/feather/feather.min.js"></script>
    <script>feather.replace()</script>
    <script src="../vendor/bootstrap/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>
