<!DOCTYPE html>
 <html>
   <head>
     <!--Import Google Icon Font-->
     <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
     <!--Import materialize.css-->
     <link type="text/css" rel="stylesheet" href="vendor/materialize/css/materialize.min.css"  media="screen,projection"/>
     <link rel="stylesheet" href="dist/css/login.css">
     <!--Let browser know website is optimized for mobile-->
     <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
     <script type="text/javascript" src="vendor/particlesjs/particles.min.js"></script>
   </head>

   <body id="bg-login">
     <!-- Your Code -->
    <div class="container login">
     <form class="col s12" action="cek_login.php" method="post">
       <div class="login-card card">
         <h4 class="center-align">Silahkan Login!</h4>
         <br>
         <div class="input-field">
           <i class="material-icons prefix">account_circle</i>
           <input id="icon_prefix" name="username" type="text" class="validate" required>
           <label for="icon_prefix">Username</label>
         </div>
         <div class="input-field">
           <i class="material-icons prefix">lock</i>
           <input id="icon_telephone" name="password" type="password" class="validate" required>
           <label for="icon_telephone">Password</label>
         </div><br>
         <button class="right btn waves-effect waves-light blue btn-large" type="submit" name="action">MASUK</button>
         <!-- notifikasi -->
         <?php
         if(isset($_GET['message'])){
           if($_GET['message'] == "login_gagal"){
             echo "<div class='right' role='alert'>Username atau password salah!</div>";
           }else if($_GET['message'] == "logout"){
             echo "<div class='right' role='alert'>Anda telah berhasil logout</div>";
           }else if($_GET['message'] == "harap_login"){
             echo "<div class='right' role='alert'>Anda harus login untuk mengakses halaman admin</div>";
           }
         }
         ?>


       </div>
       <p class="center-align">&copy; 2018 - ArnstechDev</p>
     </form>
    </div>

     <!--JavaScript at end of body for optimized loading-->
     <script type="text/javascript" src="dist/js/login.js"></script>
     <script type="text/javascript" src="vendor/materialize/js/materialize.min.js"></script>
   </body>
 </html>
